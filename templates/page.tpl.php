<div id="page-wrapper">
  <div id="page">
    <div id="header">
      <div class="section clearfix">
        <?php print $site_slogan; ?>
        <?php print render($page['header']); ?>
      </div>
    </div>
    <div id="navigation">
      <div class="section">
        <?php print $primary_nav; ?>
        <?php //print $secondary_nav; ?>
        <div id="time-marquee-wrapper">
          <div id="time-marquee">
            <?php print date('H:i:s'); ?>
          </div>
        </div>
      </div>
    </div>
    <?php print $messages; ?>
    <div id="main-wrapper">
      <div id="main" class="clearfix">
        <?php if ($page['sidebar_first']): ?>
          <div id="sidebar-first" class="column sidebar">
            <div class="section">
              <?php print render($page['sidebar_first']); ?>
            </div>
          </div>
        <?php endif; ?>
        <div id="content" class="column">
          <div class="section">
            <a id="main-content"></a>
            <?php if ($title): ?>
              <h1 class="title" id="page-title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php if ($tabs): ?>
              <div class="tabs">
                <?php print render($tabs); ?>
              </div>
            <?php endif; ?>
            <?php if ($action_links): ?>
              <ul class="action-links">
                <?php print render($action_links); ?>
              </ul>
            <?php endif; ?>
            <?php print render($page['content']); ?>
          </div>
        </div>
        <?php if ($page['sidebar_second']): ?>
          <div id="sidebar-second" class="column sidebar">
            <div class="section">
              <?php print render($page['sidebar_second']); ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div id="footer">
      <div class="section">
        <?php print render($page['footer']); ?>
      </div>
    </div>
  </div>
</div>
