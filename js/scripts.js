(function ($) {

  Drupal.behaviors.teletextScrollerKeyboard = {

    attach: function (context, settings) {

      if ($('body:not(.teletext-keyboard-processed)', context).length > 0) {
        $('body:not(.teletext-time-processed)').addClass('teletext-time-processed');
        var $scroll = $('#block-system-main>.content').innerHeight();
        var $steps = Math.floor($scroll / 300) - 1;
        var $current_step = 0;
        $(document).bind(
          'keydown',
          function(data) {
            if (data.keyCode == 40) {
              if ($current_step < $steps) {
                $current_step++;
                $('#block-system-main>.content').animate(
                  {
                    top: '-=300',
                  },
                  1000
                );
              }
              return false;
            }
            else if(data.keyCode == 38) {
              if ($current_step > 0) {
                $current_step--;
                $('#block-system-main>.content').animate(
                  {
                    top: '+=300',
                  },
                  1000
                );
              }
              return false;
            }
          }
        );
      }

    }

  };

  Drupal.behaviors.teletextTimeMarquee = {

    attach: function (context, settings) {

      var $last_flag = 1;

      setInterval(
        function() {
          var $now = new Date();
          if ($now.getHours() < 10) {
            $hours = '0' + $now.getHours();
          }
          else {
            $hours = $now.getHours();
          }
          if ($now.getMinutes() < 10) {
            $minutes = '0' + $now.getMinutes();
          }
          else {
            $minutes = $now.getMinutes();
          }
          if ($now.getSeconds() < 10) {
            $seconds = '0' + $now.getSeconds();
          }
          else {
            $seconds = $now.getSeconds();
          }
          var $text = '';
          if ($last_flag) {
            $last_flag = 0;
            $text =
              $hours
              + '<span style="visibility: hidden;">:</span>'
              + $minutes
              + '<span style="visibility: hidden;">:</span>'
              + $seconds
            ;
          }
          else {
            $last_flag = 1;
            $text = $hours + ':' + $minutes + ':' + $seconds;
          }
          $('#time-marquee').html($text);
        },
        1000
      );

    }

  };

  Drupal.behaviors.teletextBlinkText = {

    attach: function (context, settings) {

      $('.blink:not(.blink-processed)',context).each(
        function() {
          var $this = $(this);
          $this.addClass('blink-processed');
          $this.blink();
        }
      );

    }

  };

})(jQuery);
